PROGRAM PERFECT_SQUARE
    IMPLICIT NONE
    INTEGER*8 :: N, NTOT
    LOGICAL :: IS_SQUARE

    NTOT = 0
    DO N = 1, 1000000000
        IF (IS_SQUARE(N)) THEN
            NTOT = NTOT + 1
        END IF
    END DO
    WRITE (*, *) NTOT ! should find 31622 squares
END PROGRAM

LOGICAL FUNCTION IS_SQUARE(N)
    IMPLICIT NONE
    INTEGER*8 :: N, M

    ! check if ending 4 bits belong to (0,1,4,9)
    M = IAND(int(N, kind(8)), int(15, kind(8)))
    IF (.NOT. (M .EQ. 0 .OR. M .EQ. 1 .OR. M .EQ. 4 .OR. M .EQ. 9)) THEN
        IS_SQUARE = .FALSE.
        RETURN
    END IF

    ! try to find the nearest integer to sqrt(n)
    M = INT(SQRT(DBLE(N)))
    IF (M**2 .NE. N) THEN
        IS_SQUARE = .FALSE.
        RETURN
    END IF

    IS_SQUARE = .TRUE.
    RETURN
END FUNCTION
